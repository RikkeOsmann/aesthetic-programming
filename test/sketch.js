function setup() {
  createCanvas(600, 600, WEBGL);
  normalMaterial();
}
function draw() {
  background(200);
  orbitControl();
  rotateY(0.5);
  box(100, 50);
}
