//Varibale for the button:
var button;
//  variables "a" and "p" for the texts:
var a = 'You’ve won a major award!';
var p = 'You’ve won a prize! Click on the button below to claim your $500 Amazon gift card.';

let capture;

function setup(){
//setting up the canvas:
 createCanvas(windowWidth, windowHeight);
 background(233, 245, 66);

 //creating the prize button:
 button = createButton("Collect prize money!")
 button.position(500, 500);
 button.size(400, 60);
 button.style("background", "#34eb43");
 button.style("font-size", "2em");//display, color, padding, text-decoration, font-size, font-weight, border-radius, border, text-shadow, background and filter);

 //faceCapture run, when pressed:
 button.mousePressed(faceCapture);

 //major award text:
  textSize(20);
  fill(245, 66, 108);
  text(a, 500, 90, 800, 20);

  //click to win prize money text:
  textSize(15);
  fill(66, 194, 245);
  text(p, 350, 120, 900, 500);
}

function faceCapture(){
//"button" dissapear
   button.hide();


//web cam capture
   capture = createCapture(VIDEO);
   capture.size(1000, 1000);


}

function Badprize(){
//button dissapear:
    button.hide();



}
