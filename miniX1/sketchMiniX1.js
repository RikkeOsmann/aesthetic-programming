var img;
var img2;
function setup (){
  img=loadImage('pup2.png');//billede til musen (hund)
  img2=loadImage('park.jpg');//billede til baggrunden (park)
}

function draw() {
  createCanvas(800,800);//størrelse på lærred
  image(img2,400,400,800,800);//farven i baggrunden
  /*rect(mouseX,mouseY, 40, 40);//Kassen kører med musens koordinator
  rectMode(CENTER);//(dette centrer kassen i forhold til musen)*/
  image(img,mouseX,mouseY,100,100);//placering af billede i forhold til skærm
  imageMode(CENTER);

  }
