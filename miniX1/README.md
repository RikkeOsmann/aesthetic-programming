# MiniX1

[Executable](https://rikkeosmann.gitlab.io/aesthetic-programming/miniX1/)

[Repository](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX1/sketchMiniX1.js)

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX1/gif.gif)


## Puppy Power Park

Som min første minix har jeg lavet en hundepark, som jeg har opkaldt Puppy Power Park. Jeg arbejdede med HTML-websider i mine gymnasieår, så kodning er ikke helt fremmed for mig, derudover har jeg haft kurset æstetisk programmering på 2. semester (går på 4. semester nu), hvor jeg lavede min hundepark første gang. Jeg  har sidenhed haft interaktionsteknologier på 3. semester, hvor vi arbejdede en del mere med HTML, CSS og Javascript. Dajeg gik på 2. semester brugte vi det værktøj der hedder Atom, hvilket var et okay program men da jeg så skiftede til Visual Studio Code blev jeg en del gladere for selve værktøjet. Der er stor forskel på, om man starter i Atom eller Visual Studio Code (hvis man spørger mig, og hele den her indledning er en kæmpe spurgt, så det gør MAN). En af de vigtigste og bedste ting ved Visual Studio Code (for mit vedkomne) er definitionerne som er tydelige i VSC og som ikke var tilstede i Atom. 

Jeg har altid nydt både at tage og arbejde med fotografier og har gjort det i mange år. Jeg begyndte med at ændre billeder i onlineværktøjer, før jeg gik videre til Photoshop for at afprøve mine evner. Senere fandt jeg en stilling, der krævede, at jeg lavede nyhedsbreve til den virksomhed, hvor jeg arbejdede. Dette blev lavet i både Photoshop og Indesign. Begge var virkleig gode træninger for mig, fordi de satte mine talenter på prøve med hensyn til computerapplikationer og operativsystemer. Jeg besluttede at forvandle min første miniX til et sted, hvor man kan gå tur med den enorme hund Dylan på grund af min store interesse for fotografering og kærlighed til dyr. Man går tur med Dylan ved at bruge musen. Han kan desværre ikke mere end at følge musen, men måske Dylan gør et comeback i en af de andre minix'er. 

### Billeder
Jeg startede med farver, da jeg lavede den første miniX. Jeg startede med enkle baggrundsfarver og fandt ud af, hvordan man bruger pegefeltet til at interagere med skærmen. Jeg forsøgte at integrere pegefeltet med et hundebillede, men mine forsøg var stort set mislykkede. Jeg blev ved med at prøve (igen og igen og igen), og til sidst lykkedes det. Jeg havde ingen problemer med at uploade billederne til min mappe, men det blev svært for mig at tilpasse baggrundsbilledet ind i browseren efter det. Det krævede en masse tålmodighed og mange forsøg, men det lykkedes til sidst.

De fotoproblemer, jeg oplevede, var ekstremt ubelejlige og svære at rette. Jeg begyndte at kigge gennem hver linje kode i Visual Studio Code for at se, om jeg havde skrevet noget forkert, men jeg kunne ikke finde ud af, hvor problemet var. Jeg kiggede p5js.org-vejledningerne igennem, men kunne ikke finde ud af, hvor fejlen var. Så gik jeg til Google og skrev "indsæt billede i Visual Studio Code" og opdagede en YouTube-video, som jeg følte kunne og ville hjælpe mig. Jeg tog fejl igen. Jeg havde et Zoom-møde med to venner og prøvede at dele min skærm med dem for at se, om de kunne se problemet, men dette var også et mislykket forsøg. Til sidst prøvede jeg at skrive tilfældige tegn i både "create Canvas' and ```image```. Overraskende nok hjalp det og jeg var back on track.

Jeg forsøgte at kode for første gang i gymnasiet i 2. g, hvilket er 7 år siden. Jeg kan huske at lave en hjemmeside og så senere lave et spil. Da jeg lavede hjemmesiden, brugte jeg HTML, men jeg kan ikke huske hvordan eller i hvilken applikation jeg lavede det (meget rudimentære) spil. Da jeg først lærte om HTML, indså jeg, hvor vigtig strukturen er, og hvordan den læses fra top til bund. Det er muligt, at koden ikke fungerer, hvis noget er skrevet på den forkerte linje eller uden et specifikt tegn, såsom ```;```.


### Noget nyt
Jeg tror på, at kodning er både udfordrende og krævende, men at det netop er det, der gør det sjovt. Hver gang jeg lærte noget nyt i min kode, og det viste sig at være korrekt, følte jeg mig tvunget til at udvikle noget større og bedre. Jeg synes, det er sjovt at sidde sammen med andre mennesker og se på hinandens arbejde, mens man lærer af hinanden. Jeg er klar over, at kodning kan og bruges i en række forskellige industrier rundt om i verden, og jeg er spændt på at lære mere om det, så jeg kan lave bedre og mere underholdende ting. Jeg tror på, at kodning er en sjov og effektiv tilgang til at udtrykke sig. 

Som noget helt nyt nyt nyt lærte jeg om appen MIMO. Mimo fungerer lidt som Duolingo, hvor der er forskellige opgaver der skal svares på/løses. Hver gang man laver en fejl, mister man et liv (man har 5 i alt). Når alle ens liv er opbrugt skal man vente eller tilkøbe sig adgang fuldstændig som alle andre apps. Men appen lærte mig virkelig mange ting om HTML, og er klart en app jeg vil anbefale til andre nye brugere af Visual Studio Code/Atom/what ever man bruger til at skrive sin kode i. 

[YouTube (Magic Monk 2019)](https://www.youtube.com/watch?v=6rzM_RK2t3c)
