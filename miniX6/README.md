# MiniX 6 - The Matrix

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX6/1.png)


[Executable](https://rikkeosmann.gitlab.io/aesthetic-programming/miniX6/)

[Repository (matrix)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX6/matrix.js)

[Repository (pills)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX6/pills.js)


### Objekterne og deres relaterede attributter
Jeg startede spillet ved at følge samme syntaks som Winnie Soon. Jeg syntes, spillet var sjovt, og jeg tror, at jeg ville tro, at det stadig ville være en udfordring at skabe, selvom jeg havde hendes syntaks. Jeg ville selvfølgelig implementere nogle af mine ideer til dette spil, og jeg byttede hendes Tofu med røde piller. Jeg byttede også hendes Pacman med en Neo fra Matrix. Til at starte med ville jeg have taget udgangspunkt i filmen: 'Dodgeball a True Underdog Story', med Vince Vaughn og Ben Stiller i hovedrollerne. Egentlig ikke på grund af noget udover, at jeg tænkte det kunne være sjovt, at den ene side sendte bolde afsted og Vince skulle undgå boldene. Jeg fik en anden idé og det tog også udgangspunkt i film, og det var faktisk først planen, at Neo skulle undgå skud (ligesom i filmen), men jeg tænkte at det kunne være sjovt, at det var røde og blå piller, som han skulle undgå (de blå piller) og spise (de røde). Desværre løb tiden fra mig, og jeg fik kun implementeret de røde piller, som Neo skulle spise. 

Så jeg erstattede Winnie Soon's Pacman med mit billede af Neo. Bagefter fandt jeg et billede af en rød pille og erstattede hele Winnie Soons syntaks for Tofu. Så i stedet for at lave en masse piller i forskellig størrelse, tilføjede jeg et billede af en rød pille. Jeg implementerede en baggrund af en 0- og 1-taller, som i starten drillede, fordi billedet ikke ville passe på mit lærred, og generelt gav problemer i koden, men jeg fik lidt hjælp af nogle venner, og så gik det hele op i en højere enhed. Pillerne ser ud som tofuen gør i Winnie Soons spil. De adskiller sig lidt i størrelse. Som skrevet højere oppe, ville jeg gerne have haft der både var røde og blå piller, hvor de blå piller, var nogle som neo skulle undgå, men ja, tiden løb fra mig, men måske spillet er et revisit værd. 

### Objektorienteret programmering
Objektorienteret programmering, som defineret af Winnie Soon og Geoff Cox i "Aesthetic Programming: A Handbook of Software Studies," er langt mere organiseret og overskuelig. "En undersøgelse af tofu-eksemplet afslører, at objektorienteret programmering er meget organiseret og konkret, selvom objekter er abstraktioner" (Aesthetic Programming: A Handbook of Software Studies, s. 160). I JavaScript er en 'Class' et blueprint til at skabe objekter, der deler fælles egenskaber og metoder. 'Class' tilbyder en mere organiseret og intuitiv måde at skabe objektorienteret kode på, hvilken er en måde, jeg kan godt lide at gribe programmering an på den måde, da det hele virker lidt mere overskueligt og knap så forvirrende. Derudover føler jeg, at jeg har nemmere ved at forklare andre mennesker (som ikke forstår så meget om kodning) om programmering. At give et objekt nogle karakteristika for at gøre det nemt at generere flere af dem er, hvad "class" handler om, og det er det, jeg kan lide ved ´´´class´´´-delen af denne MiniX. En form for skabelon til sin kode, giver mig et bedre overblik og virker virkelig som en god idé. Udover det, så virker det også lettere at vende tilbage til og måske genbruge eller genskabe.

### Kulturel kontekst
Udover at 'Dodgeball a True Underdog Story' er en feel-good film og nem at se, var den eneste grund til at lave spillet udfra den film, at det kunne være sjovt, at spille en form for høvdingebold i mit spil. Det handlede simpelthen om, at lave et spil, der sendte noget afsted, som man så skulle undgå. Det hele udspillede sig til at blive lidt dybere. Filmen 'Matrix' er langt mere dystopisk, mørk og (i min optik) langt bedre end 'Dodgeball'. Jeg tænkte det var sjovt med det innovative koncept med en film, der udforskede ideen om virkelighed versus illusion, og hvad det vil sige at være menneske. Ideen om mennesker, der lever i en simuleret virkelighed skabt af intelligente maskiner, var en ny og tankevækkende idé, som fangede mig, da jeg så filmen for første gang. 
