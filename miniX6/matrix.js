let neoSize = {
  w: 86,
  h: 89
};
let neo;

let redpillimg;
let neoPosY;
let mini_height;
let min_pill = 7; //min røde piller på skærmen
let pill = [];
let score = 0, lose = 0;
let keyColor = 45;
let baggrund


function preload() {
  baggrund = loadImage('ny.png');//billede til baggrunden
  neo = loadImage("neo.png"); //billede til Neo
  redpillimg = loadImage("redpill.png"); // De røde piller
}


function setup() {
  createCanvas(windowWidth, windowHeight);

  neoPosY = height / 2;
  mini_height = height / 2;
}
function draw() {
  background(baggrund);
  fill(keyColor, 255);
  rect(0, height / 1.5, width, 1);
  displayScore();
  checkPillsNum(); //Tilgængelige piller
  showPill();
  image(neo, 0, neoPosY, neoSize.w, neoSize.h);
  checkEating(); //scoring
  checkResult();
}

function checkPillsNum() {
  if (pill.length < min_pill) {
    pill.push(new Pills());
  }
}

function showPill() {
  for (let i = 0; i < pill.length; i++) {
    pill[i].move();
    pill[i].show();

  }
}

function checkEating() {
  //beregner afstanden mellem hver pille og Neo
  for (let i = 0; i < pill.length; i++) {
    let d = int(
      dist(neoSize.w / 2, neoPosY + neoSize.h / 2,
        pill[i].pos.x, pill[i].pos.y)
    );
    if (d < neoSize.w / 1.4) { //Tæt nok på neo til at han spiser pillerne/ligner de bliver spist
      score++;
      pill.splice(i, 1);
    } else if (pill[i].pos.x < -5) { //Neo missed the pill
      lose++;
      pill.splice(i, 2);
    }
  }
}

function displayScore() {
  fill(keyColor, 255,250,250);
  textSize(15);
  text('You have eaten ' + score + " pill(s)", 10, height / 1.4);
  text('You have wasted ' + lose + " pill(s)", 10, height / 1.4 + 20);//+ 20 = y aksen, så den står 20 pixels længere nede.

  fill( 255, 255, 255);
  text('PRESS the ARROW UP & DOWN key to eat the red pills', 10, height / 1.4 + 40);
}

function checkResult() {
  if (lose > score && lose > 2) { //hvis dine tabte piller er højere end spiste taber du + hvis du taber med mere end 2 piller
    fill(255, 255, 255);
    textSize(20);
    text("Neo ved ikke længere hvad der er sandt eller falsk....", width / 2, height / 1.4);
    noLoop();// Spillet stopper. Fordi du har tabt.
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    neoPosY -= 50;
  } else if (keyCode === DOWN_ARROW) {//Fordi man ikke kan trykke på begge knapper samtidigt.
    neoPosY += 50;
  }
  //reset if Neo moves out of range
  if (neoY > mini_height) {
    neoPosY = mini_height;
  } else if (neoPosY < 0 - neoSize.w / 2) {
    neoPosY = 0;
  }

  // Prevent default browser behaviour
  // attached to key events.
  return false;
}
