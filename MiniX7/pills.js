let pillColors = ['red', 'blue'] 
class Pills {
    constructor()
    { //initalize the objects
    this.pillColor = random(pillColors)

    if(this.pillColor == 'red'){
      this.pillImg = redpillimg
    } else {
      this.pillImg = bluepillimg
    }
    this.speed = floor(random(3, 6));

    //check this feature: https://p5js.org/reference/#/p5/createVector

    this.pos = new createVector(width+5, random(12, height/1.7)); //Dette er hvortil pillerne bliver indsat på skærmen

    //this.size = floor(random(20, 25)); Denne del er fuldstændig ligegyldig i mit tilfælde (da det er et billede jeg har brugt)

    //rotate in clockwise for +ve no
    this.pill_rotate = random(0, PI/20); //Denne linje er også ubrugelig i min kode, for pillerne roterer ikke

    this.emoji_size = this.size/1.8;//Denne linje er heller ikke brugbar i koden. Da der er tale om et billede og ikke min egen tegning
    }
    move() {  //moving behaviors
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;//Den har en position på x-aksen. det eneste der skal ske for at den skal bevæge sig, er at den trækker den speedværdi fra dens xposition for at få den til at bevæge sig.
  }

    show() { //show pills as a picture
    image(this.pillImg,this.pos.x,this.pos.y, 50, 25);
  }
  }
