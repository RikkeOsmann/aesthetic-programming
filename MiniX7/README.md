# The Matrix Reloaded


![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/MiniX7/1.png)


[Executable](https://rikkeosmann.gitlab.io/aesthetic-programming/MiniX7/)

[Repository (Matrix)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/MiniX7/matrix.js)

[Repository (Class)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/MiniX7/pills.js)




### Hvilken miniX har du valgt?
Jeg har genbesøgt mit spil fra vores MiniX6. Efter jeg havde fremlagt foran holdet med mit Matrix-spil, blev jeg inspireret af, at lave nogle ændringer i forhold til spillet. Det ene var blandt andet, at lave spillet MERE The Matrix inspireret og derved ikke kun sende røde piller afsted fra højre mod venstre, men også sende blå piller afsted. Min protagonist og hovedperson ville stadig være Neo i venstre side, men nu skulle han ikke kun indsamle røde piller, han skulle også undgå de blå. I filmen repræsenterer de røde piller på den anden side sandheden og den barske virkelighed i situationen. Det er dem der er med til at vække Neo til det faktum, at han har levet i en computergenereret verden, og at den virkelige verden udenfor er en øde ødemark styret af maskiner. Den blå pille repræsenterer en tilbagevenden til den falske virkelighed, som Neo har levet i, hvor han mener, at verden omkring ham er virkelig, men i virkeligheden er det en simuleret computergenereret virkelighed skabt af følende maskiner for at holde mennesker føjelige og selvtilfredse. Med inspiration fra den første Matrix, hvor Neo vælger den røde pille  fordi han ønsker at bryde fri fra illusionen om Matrix og opdage sandheden om hans eksistens, har jeg nu lavet The Matrix Reloaded (ligesom den anden film i filmrækken hedder).


### Hvilke ændringer har du lavet og hvorfor?
Mine ændringer fra første spil til andet spil har egentligt hele tiden være intentionen. Jeg havde desværre bare ikke nok tid eller gode nok evner, til at fuldføre opgaverne i første omgang. Jeg tænkte fra starten, da jeg lavede mit spil, at jeg ville bruge min tid på MiniX6, fordi det klart var den MiniX, der sagde mig mest. Jeg har flere gange haft en fornemmelse af, at spillet ville være det sjoveste, at arbejde på, og syntes det også denne gang. Jeg startede som skrevet, med at gå fra kun én pille (den røde), til at have to. Den ene pille skulle være den "gode" og den anden pille den "dårlige". Når Neo (hovedpersonen) havde spist de gode piller, fik man som spiller point og når man havde spist de dårlige piller, skulle man 'dø' med det samme. Jeg prøvede at kode mig frem til, at ligeså snart man døde ville baggrunden ændre sig fra de grønne Matrix-tal til det kedeligste kontor. Et kontor, hvor man kun fik én fornemmelse af at være, og det ville være at ens energi blev ødelagt, ligeså snart man så baggrunden. Jeg var her inspireret af mockumentary serien "What We Do in the Shadows", hvor Colin Robinson har evnen til at dræne energien fra folk ved at kede dem med sine kedelige og hverdagsagtige samtaler eller ved at irritere dem med små klager og nitpicking. Så spilleren ikke ville være andet end irriteret, frustreret og kede sig. Det lykkedes desværre ikke, at frembringe 'kontor-baggrunden'. Jeg prøvede af flere forskellige omgange og med mere end én work around, men det ville simpelthen ikke virke for mig, at baggrunden skiftede til det her kontor-miljø, så snart man havde spist én blå pille. Jeg fik heldigvis styr på den blå pille blev spawned og det i sig selv, var jeg super tilfreds med. 

### Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?

Jeg har set flere af Daniel Shiffmans videoer, samt Lauren McCarthys video "Learning While Making P5 JS." Jeg huskede, hvor meget jeg nød hendes perspektiv på kodning, og hvor udfordrende og fascinerende det kunne være. Gennem alle disse MiniX-kreationer fik jeg denne følelse. Hver gang en ny MiniX ankom til ugen, var jeg lidt nervøs, især fordi mange af mine peers ser ud til at være bemærkelsesværdigt dygtige til at kode og også tror, det er enkelt. Jeg kan huske, at jeg havde svært ved at søge hjælp, fordi jeg var bekymret for at virke dum, men da jeg talte med en række af mine peers, lærte jeg, at mange af dem kæmpede med programmeringen på samme måde, som jeg gjorde (og stadig gør) .



### Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?
Altså jeg synes jo at æstetisk programmering og digital kultur hænger tæt sammen, idet de begge involverer kreativ og innovativ brug af teknologi til at producere nye former for kunst og udtryk. Hvilket klart er en fornemmelse jeg får, når jeg laver og arbejder på mine MiniX'er. Når jeg tænker på æstetiske programmering, så tænker jeg lynhurtigt på brugen af programmeringssprog, algoritmer og computerkode til at skabe digital kunst, interaktive installationer og andre former for kreative udtryk. Æstetisk programmering involverer at manipulere teknologi for at skabe visuelt betagende og interaktive kunstværker, der udvisker grænsen mellem teknologi og traditionelle kunstformer. 

Digital kultur, på den anden side, omfatter den brede vifte af kulturelle praksisser, værdier og adfærd, der er opstået som reaktion på den udbredte brug af digitale teknologier i det moderne samfund. Det omfatter de måder, hvorpå mennesker kan kommunikere, hvordan vi forbruger medier og deltager i online-fællesskaber, såvel som de måder, hvorpå teknologi har transformeret traditionelle kulturelle praksisser såsom kunst, musik og litteratur.

Æstetisk programmering er et super vigtigt element i digital kultur, da det giver 'kunstnere' og 'skabere' mulighed for at bruge teknologi til at udtrykke sig på nye og innovative måder. Digital kultur giver til gengæld en platform for æstetisk programmering for at nå et bredere publikum og blive mere integreret i mainstream praksisser. Sammen tænker jeg, at æstetisk programmering og digital kultur kan være med til at forme fremtiden for kunst og kultur i den digitale tidsalder. 
