

function draw() {
  createCanvas(800, 500);

  translate(200,40);

  fill(0,200,0)
  ellipse(40, 437, 130, 30);//left cuisse de grenouille

  fill(0,200,0)
  ellipse(160, 437, 130, 30);//right cuisse de grenouille

  fill(0,200,0);
  ellipse(100, 300, 290, 300)//body of frog

  fill(242,202,162);
  ellipse(100, 350, 150, 200);//Belly (egg)

  fill(0,200,0);
  ellipse(100, 100, 300, 150)//head

  fill(255);
  ellipse(50, 50, 100, 100);//left outer circle
  ellipse(150, 50, 100, 100);//right outer circle

  fill(0,255,150);
  ellipse(50, 50, 50, 50);//color of left eye
  ellipse(150, 50, 50, 50);//color of right eye

  fill(0);
  ellipse(50, 50, 20, 20);//left pupil
  ellipse(150, 50, 20, 20);//right pupil

  fill(0);
  ellipse(90, 100, 10, 10);//left nostril
  ellipse(110, 100, 10, 10);//right nostril

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(125, -225, 0, 100, 200, 100, 100, -225); //mouth






}
