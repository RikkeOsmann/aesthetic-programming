# Emojis

[Executable (Frog)](https://rikkeosmann.gitlab.io/aesthetic-programming/miniX2/index0.html)

[Repository (Frog)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX2/sketch.js)

[Executable (Blue person)](https://rikkeosmann.gitlab.io/aesthetic-programming/miniX2/index.html)

[Repository (Blue person)](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX2/sketch0.js)

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX2/frog.png)

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX2/blue.png)


## Introduktion


Jeg har lavet to emojis. Den første, jeg lavede, minder meget om en frø, og den anden er et blåt ansigt. Jeg skulle bruge ```curve```-funktionen for at skabe smilet på frøen. Jeg skulle bruge samme funktion senere, da jeg ville skabe de adskilte læber på den blå person. ```curve```-funktionen og jeg er ikke bedste venner. Jeg kan huske jeg brugte sindssygt lang tid på at lave den første, og da jeg endelig fandt ud af hvordan, jeg skulle skrive den, gjorde jeg det forkert igen ved den blå person. Det var en meget lang proces, som jeg flere gange overvejede at droppe og faktisk lave en helt ny emoji, fordi det blev et kæmpe irritations moment for mig. Heldigvis fik jeg hjælp fra en god ven, som åbenbart kunne gennemskue ```curve```-funktionen langt bedre end mig. Udover min struggle med curve, syntes jeg det var super hyggeligt at lave MiniX 2. Kunne godt lide bare at sætte former og farver på og bare skabe emojis ud fra, hvad der lige dukkede op i min lille hjerne.


## Kulturel kontekst

Jeg ville rigtig gerne lave to emojis som var kønsneutrale. Det skulle derfor ikke være han eller hun-betonet og det skulle ikke være noget, som man eventuelt kunne ende med at bruge, til at skade andre på den ene eller anden måde. Jeg ved godt, at det altid afhænger af modtageren og afsenderen, hvad der kan være stødende, men det var i hvert fald en ting jeg tænkte over, da jeg lavede mine to emojis. Det ville jeg gerne af forskellige årsager. Den ene årsag er: Emojis i sig selv er ikke skadelige, men deres brug kan potentielt være skadelig. Emojis kan misfortolkes, hvilket fører til misforståelser og konflikter, især i følsomme eller følelsesladede samtaler. En anden ting jeg tænkte over, da jeg lavede mine emojis var, at nogle mennesker også kan bruge emojis til at chikanere eller mobbe andre ved at bruge dem til at formidle sårende eller truende beskeder. 

En helt tredje ting, jeg endte med at tænke, var at man kan overbruge emojis, hvilket også være skadeligt, da det kan føre til tab af ægte følelsesmæssigt udtryk og kommunikation i online-interaktioner. Ikke nok med at udtryk kan miste 'ægte' følelsesmæssig udtryk, så kan det også blive set som varende uprofessionelt at sende for mange emojis. 

Da jeg lavede den første emoji, var jeg meget glad og forventningsfuld og FULD af håb. Hvilket jeg tror man godt kan se på emojien. Jeg begyndte stille og roligt med nogle ellipser, som jeg farvede i en tilfældig farve, og det gik op for mig, at basen kunne minde om begyndelsen på en frø. Jeg startede med en "ligegyldig"-mund for frøen og skiftede derefter til et lidt mere muntert udtryk. Da jeg lavede min anden emoji, altså den blå person, var mit håb og glæde for kodning nærmest opbrugt, hvilket igen nok afspejles i udtrykket. Det er ikke en specielt glad person og føler den aller helst bare gerne vil i seng og helst ikke forstyrres mere. 

Jeg bruger ikke selv særlig mange emojis, men føler de kan være en yderst nyttig funktion, hvis du skriver med en eller flere personer, du ikke kender særlig godt og har brug for at demonstrere, at det, der sms'es, er ment på en bestemt måde. Så hvad enten man er ked af det/glad/sur, kan det være lettere at udtrykke gennem emojis. 

Jeg nyder at lave ikke-binære emojis, da den følelse, som emojien kommunikerer, er den, jeg vil have folk til at forbinde den med. Hudfarve, identitet, penge og andre faktorer er således ligegyldige i mine emojis. På den anden side håber jeg inderligt, at Apple, Samsung og andre store smartphone-producenter har til hensigt at lytte til sine kunder og som et resultat heraf inkorporere de mest efterspurgte emojis, hvilket sikrer, at ingen bliver udeladt eller diskrimineret. For eksempel er jeg forvirret over, hvorfor Apple skabte (for eksempel) et piratflag, men valgte at udelukke det kurdiske flag. Jeg ved ikke, hvordan Samsung eller andre smartphone-producenter visualiserer lande/flag, eller om det overhovedet er noget, de gør, men hvis de gør, håber jeg, at de har en bedre måde end Apples nuværende situation
