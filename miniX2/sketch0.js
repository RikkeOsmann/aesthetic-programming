

function draw() {
  createCanvas(windowWidth,windowHeight);

  translate(100,40);


  fill(0,200,400);
  stroke(0);
  ellipse(400, 200, 300, 400);//head

  fill(0,100,200);
  noStroke();
  ellipse(325, 190, 140, 110);//left eye base

  fill(0,100,200);
  noStroke();
  ellipse(475, 190, 140, 110);//right eye base

  fill(255);
  stroke(0);
  ellipse(325, 185, 100, 50);//white left

  fill(255);
  stroke(0);
  ellipse(475, 185, 100, 50);//white right

  fill(0,0,0);
  stroke(255,150,255);
  strokeWeight(5);
  ellipse(345, 190, 15, 15);//pupil left

  fill(0,0,0);
  stroke(255,150,255);
  strokeWeight(5);
  ellipse(455, 190, 15, 15);//pupil right

  fill(0,200,400);
  noStroke();
  ellipse(325, 175, 100, 30);//left eyelid

  fill(0,200,400);
  noStroke();
  ellipse(475, 175, 100, 30);//right eyelid

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(420, 50, 375, 175, 275, 175, 225, 60); //left black liner

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(570, 50, 525, 175, 425, 175, 375, 60); //right black liner

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(300, 170, 275, 175, 250, 150, 200, 100); //1. left eyelash

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(325, 181, 300, 186, 275, 161, 225, 111); //2. left eyelash

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(350, 185, 325, 190, 300, 165, 250, 115); //3. left eyelash

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(500, 170, 525, 175, 550, 150, 600, 100); //1. right eyelash

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(475, 181, 500, 186, 525, 161, 575, 111); //2. right eyelash

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(450, 185, 475, 190, 500, 165, 550, 115); //3. right eyelash

  fill(0,50,150);
  stroke(0,0,0);
  ellipse(400, 340, 110, 50);//bottom lip

  fill(0,50,150);
  noStroke(0);
  ellipse(370, 320, 83, 40);//left lip

  fill(0,50,150);
  noStroke(0);
  ellipse(430, 320, 83, 40);//right lip

  noFill();
  stroke(0);
  strokeWeight(3)
  curve(600, 400, 455, 338, 345, 338, 500, 450)//bottom lip line

  translate(260,-155)
  rotate(PI / 5.0);
  fill(1000,1000,1000);
  rect(360, 328, 20, 110, 3, 10, 3, 3);//white cigaret

  fill(255,170,0);
  rect(360, 328, 20, 30, 3, 3, 0, 3, 3);//orange cigaret

}
