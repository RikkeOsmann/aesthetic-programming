

//throbber
function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (5);  //dette bestemmer hvor hurtigt min throbber snurrer
}

function draw() {
  background(10, 0, 110, 10);  //min baggrund & alpha værdien/opacity
  drawElements();//throbber snurrer på min baggrund
}

function drawElements() {
  let num = 20;//antallet af "prikker" og deres position
  push();//push and pop hører sammen. Push() bruges til at tilføje et element/element til slutningen af et array.

  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);//roterer i fuld cirkel. Syntes det var sjovt at lege med cirklen og prøve at sætte den til 180 eller 200 eller andet.
  rotate(radians(cir));
  stroke(250); //i starten kunne jeg godt lide throbberen uden strokes, men jeg synes, det gav et mere retro-look at have strokes, hvilket var det, jeg ville have i min MiniX.
  fill(10, 0, 110, 10); //matcher med drawElements i farve og opacity.
  //(250, 15, 100, 20);

  rectMode(CORNERS)
  rect(35, 0, 22, 22);
  pop();//Pop()-funktionen bruges til at slette det sidste element/element i arrayet.

/*
  translate(width/2, height/2);
  rotate(radians(cir));
  noStroke();
  fill(200, 75, 150);
  rectMode(CORNERS)
  rect(35, 0, 22, 22);

  Jeg ønskede at lave en stor throbber og derefter en større throbber uden for den mindre, men dette virkede lidt sværere end hvad jeg først forventede.
*/



}
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
