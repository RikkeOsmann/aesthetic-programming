# Hypnoterapi

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX5/1.png)

![Alt Text](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/raw/main/miniX5/2.png)

[Executable](https://rikkeosmann.gitlab.io/aesthetic-programming/miniX5/)

[Repository](https://gitlab.com/RikkeOsmann/aesthetic-programming/-/blob/main/miniX5/new.js)


## Describe how your program performs over time?

Dette program er relativt enkelt, og det er reglerne jo derfor også. Programmet fungerer ved først at skabe en simpel baggrund, hvorpå der efterfølgende tegnes rektangler (med bløde kanter) i en hvid farve. Efterhånden opstår der flere og flere bløde rektangler i den hvide farve, og det ender med at danne en form for tegning/billede/form (det hele afhænger selvfølgelig af den bruger, som ser koden køre og personens fortolkning/fantasi). Der er egentlig ikke mere til det visuelle end disse bløde rektangler udover at jeg har valgt at give dem en meget lysende blå farve i deres stroke. 

Opgaven lød på, at vi skulle have en for/while loop og en conditional statement. Jeg kæmpede i flere omgange med, hvordan jeg ville implementerer dette i min kode og endt derfor op med at smide to koder til siden, for egentlig bare, at fokusere på noget langt mere simpelt. Jeg sad og bøvlede med denne kode, fordi jeg simpelthen ikke kunne komme i tanke om noget, jeg kunne tænke mig at lave med de regler vi havde fået. Selve koden til min autogenerator er meget enkel og fylder kun 22 linjer. Dette blev pludselig en del af mit design, fordi jeg følte, det kunne være spændende at se, hvor kort jeg kunne lave en kode, der stadig kunne betragtes som en autogenerator. Jeg tænker at hvis man skal tale konceptuelt  og søger lidt (meget) bredt efter det, kan man hævde, at de mine rektangler, som tegnes i koden, løbende tegnes og på denne måde dannes til et billede. Derfor arbejder rektanglerne sammen om at producere et billede, der måske er mere komplekst. Jeg sad og prøvede at lade programmet køre i et par minutter og faldt nærmest helt hen, på grund af den hypnotiserende effekt, jeg syntes den har. 


## What role do rules and processes have in your work?:
Som jeg skrev ovenover, havde jeg meget svært ved at komme i gang og egentlig bare at være kreativ. Derfor synes jeg ikke, at reglernes rolle er vigtig. Processens rolle er lidt vigtigere, fordi noget vil blive skabt over tid. Uden nogen proces vil der ikke opstår denne form for hypnotiserende effekt og billede i sidste ende.


## The idea of “auto-generato:
Jeg fandt emnet med auto-generator meget interessant. Det faktum, at ved kun at give programmet nogle få regler, stadig kan skabe en form for noget tilfældigt, som så ender med at blive en proces og til aller sidst, et billede, og du ved ikke rigtigt nu, hvad resultatet bliver, fordi det er programmet, der udfører reglerne, som LeWitt meget godt forklarer det; “The idea becomes a machine that makes the art”(Soon & Cox). Jeg kan godt se, at mit resultat egentlig ikke er så tilfældigt, som det ser ud til, til at starte med. Men kontrollen er ikke i ens egne hænder - og det fordi du faktisk bare giver det nogle regler for, hvad programmet skal følge, men ved første øjekast kan man godt blive overrasket over, hvordan programmet følger/reagerer på disse regler. Jeg vidste for eksempel ikke før efter et par minutter, at der ligesom blev skabt de 5 cirkler udenfor den centrede rektangel og det kunne jeg ret godt lide. Et andet element, jeg godt kunne lide ved skabelsen af min kode, var den hypnotiserende del. Det fangede mig virkelig, at koden bare fik lov at køre. Selvfølgelig når alle pladserne er udfyldt, er der ikke så meget at kigge videre på, men der følte jeg også, det var tid til at komme videre. Min helt lysende lyseblå strokefarve, var noget jeg tilføjede senere, fordi jeg følte, der manglede noget, så jeg sad og legede lidt med stroke/noStroke, men blev enig med mig selv om, at den skulle have stroke og at den godt måtte have en blålig farve til den meget pinke baggrund. 


### References

#### Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

#### Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.

